import Foundation

class Api {

    private let appSettings = TRAppSettings.sharedInstance()
    private let secretMenuData = SecretMenuData()
    private let accessToken: String?

    init(accessToken: String) {
        self.accessToken = accessToken
    }

    init() {
        self.accessToken = LoginData().getAccessToken()
    }

    private var hostURL: URL {
        get {
            if (secretMenuData.useStagingApi) {
                return URL(string: "https://gitter.im/api_staging")!
            } else {
                return appSettings!.baseAPIURL()
            }
        }
    }

    func authSession() -> URLSession {
        let config = URLSessionConfiguration.ephemeral
        if let accessToken = accessToken {
          config.httpAdditionalHeaders = [ "Authorization": "Bearer \(accessToken)" ]
        }
        return URLSession(configuration: config, delegate:nil, delegateQueue:OperationQueue.main)
    }

    func urlWithPath(_ path: String!) -> URL {
        let components = URLComponents(string: hostURL.absoluteString + path)
        return components!.url!
    }

    func postRequest(_ path: String!) -> URLRequest {
        var request = URLRequest(url: urlWithPath(path))
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        return request
    }

    func putRequest(_ path: String!) -> URLRequest {
        var request = URLRequest(url: urlWithPath(path))
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "PUT"
        return request
    }

    func deleteRequest(_ path: String!) -> URLRequest {
        var request = URLRequest(url: urlWithPath(path))
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "DELETE"
        return request
    }

    func parseIf200Ok(_ data: Data?, response: URLResponse?, error: Error?) -> Any? {
        if let error = error {
            print("Api Error:", error)
            return nil
        }

        if let httpResponse = response as? HTTPURLResponse {
            guard httpResponse.statusCode == 200 else {
                print("\(httpResponse.url!) not 200 OK, received", httpResponse.statusCode)
                return nil
            }

            guard data != nil else {
                print("no data received")
                return nil
            }

            do {
                return try JSONSerialization.jsonObject(with: data!, options: [])
            } catch {
                print(error)
                return nil
            }

        } else {
            return nil
        }
    }

    func parseIf200Ok(_ completionHandler: @escaping (_ json: Any) -> Void) -> (Data?, URLResponse?, Error?) -> Void {
        return { (data: Data?, resp: URLResponse?, err: Error?) -> Void in
            if let json = self.parseIf200Ok(data, response: resp, error: err) {
                completionHandler(json)
            }
        }
    }

    func parseJsonObject(_ completionHandler: @escaping (Error?, JsonObject?) -> Void) -> (Data?, URLResponse?, Error?) -> Void {
        return { (data: Data?, resp: URLResponse?, err: Error?) -> Void in
            do {
                let rawJson = try self.parse(data, response: resp, error: err)
                guard let json = rawJson as? JsonObject else {
                    completionHandler(ApiError.failedToParseJson, nil)
                    return
                }
                completionHandler(nil, json)
            } catch {
                completionHandler(error, nil)
            }
        }
    }

    func parseJsonArray(_ completionHandler: @escaping (Error?, [JsonObject]?) -> Void) -> (Data?, URLResponse?, Error?) -> Void {
        return { (data: Data?, resp: URLResponse?, err: Error?) -> Void in
            do {
                let rawJson = try self.parse(data, response: resp, error: err)
                guard let jsonArray = rawJson as? [JsonObject] else {
                    completionHandler(ApiError.failedToParseJson, nil)
                    return
                }
                completionHandler(nil, jsonArray)
            } catch {
                completionHandler(error, nil)
            }
        }
    }

    func assertErrorless(_ completionHandler: @escaping (Error?) -> Void) -> (Data?, URLResponse?, Error?) -> Void {
        return { (data: Data?, resp: URLResponse?, err: Error?) -> Void in
            do {
                try self.assertErrorless(resp, error: err)
                completionHandler(nil)
            } catch {
                completionHandler(error)
            }
        }
    }

    private func assertErrorless(_ response: URLResponse?, error: Error?) throws {
        guard error == nil else {
            throw error!
        }

        guard let httpResponse = response as? HTTPURLResponse else {
            throw ApiError.noResponse
        }

        guard httpResponse.statusCode < 500 else {
            throw ApiError.serverError(code: httpResponse.statusCode)
        }

        guard httpResponse.statusCode < 400 else {
            throw ApiError.clientError(code: httpResponse.statusCode)
        }
    }

    private func parse(_ data: Data?, response: URLResponse?, error: Error?) throws -> Any {
        try assertErrorless(response, error: error)

        guard let data = data else {
            throw ApiError.noData
        }

        return try JSONSerialization.jsonObject(with: data, options: [])
    }

    enum ApiError: Error {
        case noResponse
        case clientError(code: Int)
        case serverError(code: Int)
        case noData
        case failedToParseJson
    }
}
