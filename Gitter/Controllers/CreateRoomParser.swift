import Foundation

class CreateRoomParser {

    private let validRoomNameCharacters: NSMutableCharacterSet

    init () {
        validRoomNameCharacters = NSMutableCharacterSet()
        validRoomNameCharacters.formUnion(with: CharacterSet.alphanumerics)
        validRoomNameCharacters.addCharacters(in: "-")
    }

    func parse(community: Group?, roomName: String?, linkedRepo: Repo?, isPrivate: Bool, orgCanJoin: Bool, githubOnly: Bool, addBadge: Bool) throws -> JsonObject {

        guard community != nil else {
            throw CreateRoomParserError.communityMissing
        }

        guard roomName != nil else {
            throw CreateRoomParserError.roomNameMissing
        }

        guard roomName!.rangeOfCharacter(from: validRoomNameCharacters.inverted) == nil else {
            throw CreateRoomParserError.roomNameInvalid
        }

        var security: JsonObject = [
            "security": isPrivate ? "PRIVATE" : "PUBLIC" as Any
        ]

        if let linkedRepo = linkedRepo {
            security["type"] = "GH_REPO" as AnyObject?
            security["linkPath"] = linkedRepo.uri as AnyObject?
        } else if (orgCanJoin || !isPrivate) {
            security["type"] = community?.backedBy_type as AnyObject?
            security["linkPath"] = community?.backedBy_linkPath as AnyObject?
        }

        var body: JsonObject = [
            "name": roomName! as AnyObject,
            "security": security as AnyObject,
            "addBadge": addBadge as AnyObject
        ]

        if githubOnly {
            body["providers"] = ["github"]
        }

        return body
    }
}

enum CreateRoomParserError: Error {
    case communityMissing
    case roomNameMissing
    case roomNameInvalid
}
